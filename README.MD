# Hacker news mobile client
* Mobile client for the popular site [HackerNews](https://news.ycombinator.com/)
* Written in xamarin forms.
* Tested on Android and UWP, iOS nope.

# Libraries
* Acr UserDialogs -> used for showing alerts, popups...
* ReactiveUI -> MVVM framework
* Sextant -> Navigation library
* Plugin.Permissions -> for requiring device specific permissions (camera, storage, ...)
* Plugin.Connectivity -> check connection status of the device, gather connection type, bandwidths, ...
* HtmlAgilityPack -> used for parsing HTML tags
* Refit -> The automatic type-safe REST library for .NET Core, Xamarin and .NET.
* DynamicData -> is a library which brings the power of Reactive Extensions (Rx) to collections.